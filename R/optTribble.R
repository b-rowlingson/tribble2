##'  Computes the log likelihood for the tribble model, but in a form
##' suitable calling from optim().
##'
##' Log-likelihood computation
##' 
##' @title tribble function for optim
##' @param pars Vector of parameters - rho, alpha, beta, theta
##' @param ccflag Case/control flag
##' @param distances Matrix of distances
##' @param which Mapping of distances to alpha and beta parameters
##' @param covars Matrix of covariates
##' @return log-likelihood value
##' @author Barry Rowlingson
##' @seealso tribbleNG
##' @export
"optTribble" <-
function(pars, ccflag, distances, which, covars){
###
### tribble log-likelihood in a form for the optim function
### - so all parameters are in one vector, pars
###

### first break it down

  rho <- pars[1]

  
  if(!is.null(distances)){
    ndVars <- max(which)
    alphas <- pars[2:(ndVars+1)]
    betas <- pars[(ndVars+2):(2*ndVars + 1)]
  }else{
    ndVars <- 0
    alphas <- betas <- NULL
  }


  if(!is.null(covars)){
    ncVars <- length(pars)-ndVars-1
    thetas <- pars[(2*ndVars+2):length(pars)]
  }else{
    ncVars <- 0
    thetas <- NULL
  }

  -logLikTribble(ccflag, rho, distances, alphas, betas, which, covars, thetas)
  
}
