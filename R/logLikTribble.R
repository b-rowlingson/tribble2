"logLikTribble" <-
function(ccflag, rho, distances=NULL, alphas, betas, which, covars=NULL, thetas){

  fm <- rep(1,length(ccflag))

  if(!is.null(distances)){   
    alphaV <- alphas[which]
    betaV <- betas[which]

    for(i in 1:length(which)){
      fm <- fm * ( 1+alphaV[i]*exp(-(distances[,i]/betaV[i])^2) )
    }

  }

  if(!is.null(covars)){
    scv <- apply(thetas %*% t(covars),2,sum)
    fm <- fm*exp(scv)
  }

  p=(rho*fm)/(1+rho*fm)
  pcase <- p[ccflag==1]
  pcont <- p[ccflag==0]
  return(sum(c(log(pcase),log(1-pcont))))
  

}
