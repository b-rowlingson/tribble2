###
### preparing data from sp classes
###

prepSP <- function(sp, source_location, caseformula){
    
    xyd = sp::coordinates(sp)
    xys = sp::coordinates(source_location)

    d2 = (xyd[,1]-xys[,1])^2 + (xyd[,2]-xys[,2])^2

    cc = stats::model.frame(data=sp@data, formula=caseformula)[,1]
    cc = as.numeric(cc)
    data.frame(distance = sqrt(d2), cc = cc)
    
}
